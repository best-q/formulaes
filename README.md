# Instalation

 - Add new brew tap:
   ```
   brew tap bestq/formulaes git@bitbucket.org:best-q/formulaes.git
   ```
 - To install any formula from this repository you can do:
   ```
   brew install <FORMULA>
   ```
   or
   ```
   brew install bestq/formulaes/<FORMULA>
   ```
   if naming collision

#Formulaes

[localizer](https://bitbucket.org/best-q/localizer/) for Swift projects.
