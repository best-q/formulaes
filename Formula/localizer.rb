class Localizer < Formula
  desc "localizer for Swift projects"
  homepage "https://bitbucket.org/best-q/localizer/"
  url "git@bitbucket.org:best-q/localizer.git",
      :using => :git,
      :tag => "V1.7",
      :revision => "fe414b81bd57bdf1dc3f288fb118ca3a3b4c6f50"
  head "git@bitbucket.org:best-q/localizer.git"

  depends_on :xcode => ["10.2", :build]

  def install
    system "make", "install", "prefix=#{prefix}"
  end

  test do
    system "#{bin}/localizer"
  end
end
